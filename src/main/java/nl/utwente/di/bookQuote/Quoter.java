package nl.utwente.di.bookQuote;

import java.util.HashMap;
import java.util.Map;

public class Quoter {
    private Map<String,Double> bookPrices= new HashMap<>();

    public void setBookPrices(String isbn, Double price) {
        this.bookPrices.put(isbn, price);
    }

    public double getBookPrice(String isbn) {
        setBookPrices(String.valueOf(1), 10.0);
        setBookPrices(String.valueOf(2), 45.0);
        setBookPrices(String.valueOf(3), 20.0);
        setBookPrices(String.valueOf(4), 35.0);
        setBookPrices(String.valueOf(5), 50.0);

        if (bookPrices.containsKey(isbn)) {
            return bookPrices.get(isbn);
        }
        return 0.0;
    }
}
